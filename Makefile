CC=g++
CFLAGS= -lwiringPi
LDFLAGS= -lwiringPi -lpthread
SOURCES=RasPiSat.cpp
OBJECTS=RasPiSat.cpp
EXECUTABLE=RasPiSat

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
