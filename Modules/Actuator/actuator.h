// Actuator Module
// Sets actuator to work when it is properly signaled by the other modules

// Headers for specific actuator functions
void ActivateAllMutex();
void DectivateAllMutex();

void *Actuator(void *parm)
{
    while (1) // Thou shall never end
    {
        sem_wait(&act_queue); // Wait until someone calls in
        
        ActivateAllMutex(); // Locking all variables from writing
        
        /******************* Set priorities list ********************************/
        if (var.act.shall.crit_over) // If actuator shall critical override
        {
            // Broadcast actuator is actuating
            var.act.is_active = true;
            
            // Who is actuating
            var.act.has.crit_over = true;
            var.act.has.comm_do = false;
            var.act.has.comm_tt = false;
            var.act.has.comm_orb = false;
            
            // Reset requester
            var.act.shall.crit_over = false;
            
            printf("ACT: Requested actuation from Critical Override\n");
        }
        else if (var.act.shall.comm_do) // If actuator shall do
        {
            if (!var.act.has.crit_over) // Make do command
            {
                // Broadcast actuator is actuating
                var.act.is_active = true;
                
                // Who is actuating
                var.act.has.crit_over = false;
                var.act.has.comm_do = true;
                var.act.has.comm_tt = false;
                var.act.has.comm_orb = false;
                
                // Reset requester
                var.act.shall.comm_do = false;
                
                printf("ACT: Requested actuation from COMMS\n");
            }
        }
        else if (var.act.shall.comm_tt) // If actuator shall tt
        {
            if (!var.act.has.crit_over && !var.act.has.comm_do) // Make tt command
            {
                // Broadcast actuator is actuating
                var.act.is_active = true;
                
                // Who is actuating
                var.act.has.crit_over = false;
                var.act.has.comm_do = false;
                var.act.has.comm_tt = true;
                var.act.has.comm_orb = false;
                
                // Reset requester
                var.act.shall.comm_tt = false;
                
                printf("ACT: Requested actuation from TimeTag\n");
            }
        }
		else if (var.act.shall.comm_orb) // If actuator shall orb command
        {
            if (!var.act.has.crit_over && !var.act.has.comm_do && !var.act.has.comm_tt) // Make orb command
            {
                // Broadcast actuator is actuating
                var.act.is_active = true;
                
                // Who is actuating
                var.act.has.crit_over = false;
                var.act.has.comm_do = false;
                var.act.has.comm_tt = false;
                var.act.has.comm_orb = true;
                
                // Reset requester
                var.act.shall.comm_orb = false;
                
                printf("ACT: Requested actuation from ORBIT\n");
            }
        }

        DectivateAllMutex();
        
        /******************* Set actuator commands ********************************/
        if (var.act.is_active)
        {
            // Set LED on
            Lit_LED(true,var.act.gpio_led_act);
            // Wait 2 seconds
            sleep(2);
            // Set LED off
            Lit_LED(false,var.act.gpio_led_act);
            
            // Set actuator as not actuating
            pthread_mutex_lock(&mutx.act.is_active);
            var.act.is_active = false;
            pthread_mutex_unlock(&mutx.act.is_active);
            
            // Reset all actuations
            var.act.has.crit_over = false;
            var.act.has.comm_do = false;
            var.act.has.comm_tt = false;
            var.act.has.comm_orb = false;
        }
        
    }
    
}

void ActivateAllMutex()
{
    /* Function that locks all the mutex implied in the actuator.
       Separated for better code understanding. */
    pthread_mutex_lock(&mutx.act.is_active);
    pthread_mutex_lock(&mutx.act.shall.crit_over);
    pthread_mutex_lock(&mutx.act.shall.comm_do);
    pthread_mutex_lock(&mutx.act.shall.comm_tt);
    pthread_mutex_lock(&mutx.act.shall.comm_orb);
}

void DectivateAllMutex()
{
    /* Function that unlocks all the mutex implied in the actuator.
     Separated for better code understanding. */
    pthread_mutex_unlock(&mutx.act.is_active);
    pthread_mutex_unlock(&mutx.act.shall.crit_over);
    pthread_mutex_unlock(&mutx.act.shall.comm_do);
    pthread_mutex_unlock(&mutx.act.shall.comm_tt);
    pthread_mutex_unlock(&mutx.act.shall.comm_orb);
}