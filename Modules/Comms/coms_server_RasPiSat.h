// Communications module for RasPiSat
//
// Maintains a TCP/IP server and listens for a stream.
// Takes an action according to the interpreter commands in interpreter function.

// Libraries
#include "tcp_server.h"		// TCPServer class
#include "interpreter.h"    // Interpreter function

void *CommsServer(void *parm)
{
	
	// Declare Variables
	size_t len; // Length of the received stream
	char line[256]; // Data from the received stream
    string reply; // Data to be send to stream
	bool server_enabled = true; // Keep the server enabled
    TCPServer* server = NULL; // Server class (not initialized)
    TCPStream* stream = NULL; // DataStream class (not initialized)

	// Set Hardware Initial Status
	Lit_LED(false,var.gpio_led_comms); //Led is OFF as the communications are closed
	
	// Create New server
    server = new TCPServer(4040);
	
	// Start Server
    if (server->start() == 0)
	{
        while (server_enabled) // Keep the server enabled
		{
			// Wait for an client connection and get the incoming data stream
            stream = server->accept();
			
			// Process data stream
            if (stream != NULL) // A valid data stream has been received
			{
                Lit_LED(true,var.gpio_led_comms); // Led is ON as the communication has been stablished
                
				// This code must be particularized for each server application
                while ((len = stream->receive(line, sizeof(line))) > 0)
				{
                    line[len] = '\0'; // Termination character
                    
					// Print stream and bounce it back to the client
					reply = interpreter(line); // Print received string
                    
                    stream->send(reply.c_str(), reply.length()+1); // Send response to the Client
                }
                
                Lit_LED(false,var.gpio_led_comms); //Led is OFF as  the communications are closed
            }
			
			else // A NULL data stream has been received
			{
				// Optional code to process a NULL stream scenario
			}
        }
    }
	
	else // ERROR. Server could not be initialized
	{
        printf("ERROR. Server could not be initialized\n"); // Info Message
        exit(1); // Terminate Program with error exit code
	}
	
	// Terminate Program with success exit code
    exit(0);
}

