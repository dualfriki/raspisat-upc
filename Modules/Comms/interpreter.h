// Interpreter
//
// TCP/IP command translator and actuator

//Headings
string GetStatusString();

string interpreter(char command[256])
{
/*
Available commands:
 - status: dump of all the satellite info
 - do: forces the satellite to actuate
*/
    char * order;
    char cmd[256];
    
    // Splitting command
    order = strtok (command," ");
    
     strcpy(cmd,order);
    
    // Order processing
    if (strcmp(cmd,"hello") == 0) // Reference command to check TCP/IP socket is active
    {
        return "hi";
    }
    else if (strcmp(cmd,"status") == 0) // Gets RasPiSat status and broadcasts through TCP/IP
    {
        return GetStatusString();
    }
    else if (strcmp(cmd,"do") == 0) // Forces the satellite to actuate in the same instant
    {
        // Crop the string to get info, currently not implemented
        while (order != NULL)
        {
            order = strtok (NULL, " ");
        }
        
        // Set command to actuator and increment semaphore value
        pthread_mutex_lock(&mutx.act.shall.comm_do);
        var.act.shall.comm_do = true;
        sem_post(&act_queue);
        pthread_mutex_unlock(&mutx.act.shall.comm_do);
        
        return "Command processed";
    }
    else if (strcmp(cmd,"tt") == 0) // Stores in the satellite DB a time instant where it shall actuate
    {
        // Crop the string to get info, currently not implemented
        while (order != NULL)
        {
            order = strtok (NULL, " ");
        }
        
        /* Timetags are currently NOT implemented*/
        
        return "Timetag processed";
    }
    else if (strcmp(cmd,"exit") == 0) // Closes TCP/IP connection
    {
        return "Connection closed";
    }
    else                              // In any other case, command is not understood
    {
        return "Command not found";
    }
}

string GetStatusString()
/* Function that returns a string with the status of the satellite */
{
    string status = "Status command received\n"; char prompt[256];
    int queue;
    
	// Show Time
	char buffer[4096];
	strftime (buffer,4096,"Date: %Z %c\n",gmtime(&var.t.last));
	status.append(buffer);

	// Header
    status.append("Magnitude: Sensor | Nominal | Offset\n");
    
    // Pitch
    pthread_mutex_lock(&mutx.svs.pitch);
    pthread_mutex_lock(&mutx.svn.pitch);
    sprintf(prompt,"Pitch: %.2f | %.2f | %.2f\n",
            var.svs.pitch,var.svn.pitch,var.svs.pitch-var.svn.pitch);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.pitch);
    pthread_mutex_unlock(&mutx.svs.pitch);
    
    // Roll
    pthread_mutex_lock(&mutx.svs.roll);
    pthread_mutex_lock(&mutx.svn.roll);
    sprintf(prompt,"Roll: %.2f | %.2f | %.2f\n",
            var.svs.roll,var.svn.roll,var.svs.roll-var.svn.roll);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.roll);
    pthread_mutex_unlock(&mutx.svs.roll);
    
    // Yaw
    pthread_mutex_lock(&mutx.svs.yaw);
    pthread_mutex_lock(&mutx.svn.yaw);
    sprintf(prompt,"Yaw: %.2f | %.2f | %.2f\n",
            var.svs.yaw,var.svn.yaw,var.svs.yaw-var.svn.yaw);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.yaw);
    pthread_mutex_unlock(&mutx.svs.yaw);
    
    // w1
    pthread_mutex_lock(&mutx.svs.w1);
    pthread_mutex_lock(&mutx.svn.w1);
    sprintf(prompt,"w1: %.2f | %.2f | %.2f\n",
            var.svs.w1,var.svn.w1,var.svs.w1-var.svn.w1);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.w1);
    pthread_mutex_unlock(&mutx.svs.w1);
    
    // w2
    pthread_mutex_lock(&mutx.svs.w2);
    pthread_mutex_lock(&mutx.svn.w2);
    sprintf(prompt,"w2: %.2f | %.2f | %.2f\n",
            var.svs.w2,var.svn.w2,var.svs.w2-var.svn.w2);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.w2);
    pthread_mutex_unlock(&mutx.svs.w2);
    
    // w3
    pthread_mutex_lock(&mutx.svs.w3);
    pthread_mutex_lock(&mutx.svn.w3);
    sprintf(prompt,"w3: %.2f | %.2f | %.2f\n",
            var.svs.w3,var.svn.w3,var.svs.w3-var.svn.w3);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.w3);
    pthread_mutex_unlock(&mutx.svs.w3);
    
    // vx
    pthread_mutex_lock(&mutx.svs.vx);
    pthread_mutex_lock(&mutx.svn.vx);
    sprintf(prompt,"vx: %.2f | %.2f | %.2f\n",
            var.svs.vx,var.svn.vx,var.svs.vx-var.svn.vx);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.vx);
    pthread_mutex_unlock(&mutx.svs.vx);
    
    // vy
    pthread_mutex_lock(&mutx.svs.vy);
    pthread_mutex_lock(&mutx.svn.vy);
    sprintf(prompt,"vy: %.2f | %.2f | %.2f\n",
            var.svs.vy,var.svn.vy,var.svs.vy-var.svn.vy);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.vy);
    pthread_mutex_unlock(&mutx.svs.vy);
    
    // vz
    pthread_mutex_lock(&mutx.svs.vz);
    pthread_mutex_lock(&mutx.svn.vz);
    sprintf(prompt,"vz: %.2f | %.2f | %.2f\n",
            var.svs.vz,var.svn.vz,var.svs.vz-var.svn.vz);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.vz);
    pthread_mutex_unlock(&mutx.svs.vz);
    
    // x
    pthread_mutex_lock(&mutx.svs.x);
    pthread_mutex_lock(&mutx.svn.x);
    sprintf(prompt,"x: %.2f | %.2f | %.2f\n",
            var.svs.x,var.svn.x,var.svs.x-var.svn.x);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.x);
    pthread_mutex_unlock(&mutx.svs.x);
    
    // y
    pthread_mutex_lock(&mutx.svs.y);
    pthread_mutex_lock(&mutx.svn.y);
    sprintf(prompt,"y: %.2f | %.2f | %.2f\n",
            var.svs.y,var.svn.y,var.svs.y-var.svn.y);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.y);
    pthread_mutex_unlock(&mutx.svs.y);
    
    // z
    pthread_mutex_lock(&mutx.svs.z);
    pthread_mutex_lock(&mutx.svn.z);
    sprintf(prompt,"z: %.2f | %.2f | %.2f\n",
            var.svs.z,var.svn.z,var.svs.z-var.svn.z);
    status.append(prompt);
    pthread_mutex_unlock(&mutx.svn.z);
    pthread_mutex_unlock(&mutx.svs.z);
    
    // Actuator
    sem_getvalue(&act_queue,&queue);
    sprintf(prompt,"Actuator queue: %d\n",queue);
    status.append(prompt);
    pthread_mutex_lock(&mutx.act.is_active);
    if (var.act.is_active) {
        status.append("Actuating: true");
    }else{
        status.append("Actuating: false");
    }
    pthread_mutex_unlock(&mutx.act.is_active);
    
    // Return
    return status;
}