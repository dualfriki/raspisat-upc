// Variables class for RasPiSat
//
// Class name is "Variables"
// GLOBAL Variable name is "var"
//
// Class can be initialized with: var.Initialize();
// Initialization forces default values and updates
// values from a "config.cfg" namelist file, if exists.


class Variables
{
public:

	// Orbit Control
	struct StateVector
	{
		double pitch;
		double roll;
		double yaw;
		double w1;
		double w2;
		double w3;
		double vx;
		double vy;
		double vz; 
		double x;
		double y;
		double z;
	} svs, svn, svo, svt; // Sensor, Nominal, Offset

	// Actuator
	struct Actuator
	{
		bool is_active;
		int gpio_led_act;

		struct Status
		{
			bool crit_over;
			bool comm_do;
			bool comm_tt;
			bool comm_orb;
		} has, shall;

	} act;

	// Time Tag
	struct TimeTag
	{
		int t;
		string mov;
		double value;
		int duration;
	} tt;

	// TCP-IP Server
	string ip;
	int port;
	int gpio_led_comms;

	// Clock
	struct Timing
	{
		time_t last;
		time_t next;
		time_t offset;
	} t;

	// Constructor
	Variables()
	{
		load_defaults();
		load_cfg();
	}
	
private:
	
	// Initialize State Vector variables to default values
	void svdef ( StateVector sv )
	{
		sv.x = 0; sv.y = 0; sv.z = 0;
		sv.vx = 0; sv.vy = 0; sv.vz = 0;
		sv.w1 = 0; sv.w2 = 0; sv.w3 = 0;
		sv.pitch = 0; sv.roll = 0; sv.yaw = 0;
	}
	
	// Initialize Time Tag variables to default values
	void ttdef ( TimeTag tt )
	{
		tt.t = 0;
		tt.mov="";
		tt.value = 0;
		tt.duration = 0;
	}
	
	// Initialize variables to default values
	void load_defaults ( )
	{
		// Orbit Control
		svdef(svs);
		svdef(svn);
		svdef(svo);
		svdef(svt);

		// Actuator
		act.is_active = false;
		act.gpio_led_act = 4;
		act.has.crit_over = false;
		act.has.comm_do = false;
		act.has.comm_tt = false;
		act.has.comm_orb = false;
		act.shall.crit_over = false;
		act.shall.comm_do = false;
		act.shall.comm_tt = false;
		act.shall.comm_orb = false;

		// Time Tag
		ttdef(tt);

		// TCP-IP Server
		port = 4040;
		ip = "localhost";
		gpio_led_comms = 0;
		
		// Clock
		t.offset = 5;
 
	}

	void load_cfg()
	{
		// Auxiliary variables
		string name, darkest_void;
		double threshold_value = 0;

		// Input File
		ifstream file;
		file.open ("config.cfg", ifstream::in);

		// File is accessible
		while(file.good())
		{
			// Comment Line
			while(file.peek()=='*') file.ignore(256,'\n'); // Ignore until \n

			// Variable Line
			file>>name; // Get variable name

				// TEST
				if(name.compare("TEST")==0) file>>darkest_void;

				// Actuator
				else if(name.compare("act.gpio_led_act")==0) file>>act.gpio_led_act;

				// Comms
				else if(name.compare("ip")==0) file>>ip;
				else if(name.compare("port")==0) file>>port;
				else if(name.compare("gpio_led_comms")==0) file>>gpio_led_comms;
				
				// Threshold
				else if(name.compare("threshold_value")==0) file>>threshold_value;
				else if(name.compare("svt.pitch")==0) file>>svt.pitch;
				else if(name.compare("svt.yaw")==0) file>>svt.yaw;
				else if(name.compare("svt.roll")==0) file>>svt.roll;
				else if(name.compare("svt.w1")==0) file>>svt.w1;
				else if(name.compare("svt.w2")==0) file>>svt.w2;
				else if(name.compare("svt.w3")==0) file>>svt.w3;

				// Unknown
				else file>>darkest_void;
		}

		// Apply threshold_value
		svt.pitch	= svt.pitch		* threshold_value;
		svt.yaw		= svt.yaw		* threshold_value;
		svt.roll	= svt.roll		* threshold_value;
		svt.w1		= svt.w1		* threshold_value;
		svt.w2		= svt.w2		* threshold_value;
		svt.w3		= svt.w3		* threshold_value;
	}

} var;
