// Mutex class for RasPiSat and Threading
//
// Threads are orbit control (crtl), actuator (act), communications (comms) and watchdog (wdg)
//
// Semaphore simulates the queue in the actuator (act_queue)
//
// Class name is "Mutex"
// GLOBAL Variable name is "mutx"

// Thread definition
pthread_attr_t attr;
pthread_t crtl, act, coms, wdg;

// Semaphore definition
sem_t act_queue;

// Mutex definition
class Mutex
{
public:
    
    // Orbit Control
    struct StateVector
    {
        pthread_mutex_t pitch;
        pthread_mutex_t roll;
        pthread_mutex_t yaw;
        pthread_mutex_t w1;
        pthread_mutex_t w2;
        pthread_mutex_t w3;
        pthread_mutex_t vx;
        pthread_mutex_t vy;
        pthread_mutex_t vz;
        pthread_mutex_t x;
        pthread_mutex_t y;
        pthread_mutex_t z;
    }svs, svn;
    
    //Actuator
    struct Actuator
    {
        pthread_mutex_t is_active;
        struct Status
        {
            pthread_mutex_t crit_over;
            pthread_mutex_t comm_do;
            pthread_mutex_t comm_tt;
            pthread_mutex_t comm_orb;
        }has,shall;
        
    }act;
    
	// Orbit
	pthread_mutex_t orbit_mutex;
	pthread_cond_t orbit_cond;
    
    Mutex()
    {
        setattributes();
    }
    
private:
    
    void setattributes()
    {
        //Initialize mutex attributes
        pthread_mutex_init(&svs.pitch,NULL);
        pthread_mutex_init(&svs.roll,NULL);
        pthread_mutex_init(&svs.yaw,NULL);
        pthread_mutex_init(&svs.w1,NULL);
        pthread_mutex_init(&svs.w2,NULL);
        pthread_mutex_init(&svs.w3,NULL);
        pthread_mutex_init(&svs.vx,NULL);
        pthread_mutex_init(&svs.vy,NULL);
        pthread_mutex_init(&svs.vz,NULL);
        pthread_mutex_init(&svs.x,NULL);
        pthread_mutex_init(&svs.y,NULL);
        pthread_mutex_init(&svs.z,NULL);
        
        pthread_mutex_init(&svn.pitch,NULL);
        pthread_mutex_init(&svn.roll,NULL);
        pthread_mutex_init(&svn.yaw,NULL);
        pthread_mutex_init(&svn.w1,NULL);
        pthread_mutex_init(&svn.w2,NULL);
        pthread_mutex_init(&svn.w3,NULL);
        pthread_mutex_init(&svn.vx,NULL);
        pthread_mutex_init(&svn.vy,NULL);
        pthread_mutex_init(&svn.vz,NULL);
        pthread_mutex_init(&svn.x,NULL);
        pthread_mutex_init(&svn.y,NULL);
        pthread_mutex_init(&svn.z,NULL);
        
        pthread_mutex_init(&act.is_active,NULL);
        pthread_mutex_init(&act.has.crit_over,NULL);
        pthread_mutex_init(&act.has.comm_do,NULL);
        pthread_mutex_init(&act.has.comm_tt,NULL);
        pthread_mutex_init(&act.has.comm_orb,NULL);
        pthread_mutex_init(&act.shall.crit_over,NULL);
        pthread_mutex_init(&act.shall.comm_do,NULL);
        pthread_mutex_init(&act.shall.comm_tt,NULL);
        pthread_mutex_init(&act.shall.comm_orb,NULL);
		
        pthread_mutex_init(&orbit_mutex,NULL);
    }
    
    
}mutx;
