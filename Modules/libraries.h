// RasPiSat required Libraries

// Standard Libraries
#include <stdio.h>			// Standard Input-Output
#include <stdlib.h>			// Standard Library
#include <unistd.h>			// Unix Standard Library
#include <pthread.h>        // POSIX Threads library
#include <string>			// String Library
#include <fstream>			// File Stream
#include <math.h>			// Math Library
#include <time.h>           // Time library
#include <semaphore.h>      // Semaphore Library

// Standard Namespace
using namespace std;

// Custom Variables
#include "Variables/variables.h"		// Global variables class
#include "Variables/threading.h"        // Main threading and mutex definitions

