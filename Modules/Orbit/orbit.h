// Orbit Module
// Gets information from the sensors and correlates it with a nominal orbit.
// Calculates the offset error between the sensor orbit and the nominal orbit.

// Define Subroutines
void *Read_Sensor(void*parm);
void *Read_Nominal(void*parm);
void Calculate_Offset(void);
void Set_Actuator(void);

// Main Function
void *Orbit(void*parm)
{
	// Lock mutex
	pthread_mutex_lock(&mutx.orbit_mutex);

    // Variables
	pthread_attr_t attr; // Attributes
	pthread_attr_init(&attr); // Initialize Attributes
    pthread_t tr_Read_Sensor, tr_Read_Nominal; // Thread handles

	// Counter
	int i = 0; // Must be improved: Skip lines in text file.
    
	while ( true ) // Needs error control on "end of data".
	{
		// Wait for Watchdog signal
		pthread_cond_wait(&mutx.orbit_cond, &mutx.orbit_mutex);

		// Create threads to Read Orbital Data
		pthread_create(&tr_Read_Sensor, &attr, Read_Sensor, (void*) &i);
		pthread_create(&tr_Read_Nominal, &attr, Read_Nominal, (void*) &i);

		// Join Threads
		pthread_join(tr_Read_Sensor, NULL);
		pthread_join(tr_Read_Nominal, NULL);

		// Calculate Offset
		Calculate_Offset();

		// Set Actuator Status
		Set_Actuator();
		
		// Increment counter
		i++;

	}
    
	// Unlock mutex
	pthread_mutex_unlock(&mutx.orbit_mutex);

}

void *Read_Sensor(void *iter)
{
	// Get index
	int i = *(int *) iter;
	int k = 0;

	// Read Source
	ifstream source;
	source.open("Data/pitchp.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.pitch);	source >> var.svs.pitch;	pthread_mutex_unlock(&mutx.svs.pitch);	}	source.close();
	source.open("Data/rollp.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.roll);		source >> var.svs.roll;		pthread_mutex_unlock(&mutx.svs.roll);	}	source.close();
	source.open("Data/yawp.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.yaw);		source >> var.svs.yaw;		pthread_mutex_unlock(&mutx.svs.yaw);	}	source.close();
	source.open("Data/w1p.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.w1);		source >> var.svs.w1;		pthread_mutex_unlock(&mutx.svs.w1);		}	source.close();
	source.open("Data/w2p.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.w2);		source >> var.svs.w2;		pthread_mutex_unlock(&mutx.svs.w2);		}	source.close();
	source.open("Data/w3p.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.w3);		source >> var.svs.w3;		pthread_mutex_unlock(&mutx.svs.w3);		}	source.close();
	source.open("Data/vxp.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.vx);		source >> var.svs.vx;		pthread_mutex_unlock(&mutx.svs.vx);		}	source.close();
	source.open("Data/vyp.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.vy);		source >> var.svs.vy;		pthread_mutex_unlock(&mutx.svs.vy);		}	source.close();
	source.open("Data/vzp.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.vz);		source >> var.svs.vz;		pthread_mutex_unlock(&mutx.svs.vz);		}	source.close();
	source.open("Data/xp.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.x);		source >> var.svs.x;		pthread_mutex_unlock(&mutx.svs.x);		}	source.close();
	source.open("Data/yp.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.y);		source >> var.svs.y;		pthread_mutex_unlock(&mutx.svs.y);		}	source.close();
	source.open("Data/zp.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svs.z);		source >> var.svs.z;		pthread_mutex_unlock(&mutx.svs.z);		}	source.close();
	
}

void *Read_Nominal(void *iter)
{
	// Get index
	int i = *(int *) iter;
	int k = 0;

	// Read Source
	ifstream source;
	source.open("Data/pitch.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.pitch);	source >> var.svn.pitch;	pthread_mutex_unlock(&mutx.svn.pitch);	}	source.close();
	source.open("Data/roll.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.roll);		source >> var.svn.roll;		pthread_mutex_unlock(&mutx.svn.roll);	}	source.close();
	source.open("Data/yaw.txt");	if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.yaw);		source >> var.svn.yaw;		pthread_mutex_unlock(&mutx.svn.yaw);	}	source.close();
	source.open("Data/w1.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.w1);		source >> var.svn.w1;		pthread_mutex_unlock(&mutx.svn.w1);		}	source.close();
	source.open("Data/w2.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.w2);		source >> var.svn.w2;		pthread_mutex_unlock(&mutx.svn.w2);		}	source.close();
	source.open("Data/w3.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.w3);		source >> var.svn.w3;		pthread_mutex_unlock(&mutx.svn.w3);		}	source.close();
	source.open("Data/vx.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.vx);		source >> var.svn.vx;		pthread_mutex_unlock(&mutx.svn.vx);		}	source.close();
	source.open("Data/vy.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.vy);		source >> var.svn.vy;		pthread_mutex_unlock(&mutx.svn.vy);		}	source.close();
	source.open("Data/vz.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.vz);		source >> var.svn.vz;		pthread_mutex_unlock(&mutx.svn.vz);		}	source.close();
	source.open("Data/x.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.x);		source >> var.svn.x;		pthread_mutex_unlock(&mutx.svn.x);		}	source.close();
	source.open("Data/y.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.y);		source >> var.svn.y;		pthread_mutex_unlock(&mutx.svn.y);		}	source.close();
	source.open("Data/z.txt");		if(source.good()) {	for(k=0;k<=i;k++) source.ignore(256,'\n');	pthread_mutex_lock(&mutx.svn.z);		source >> var.svn.z;		pthread_mutex_unlock(&mutx.svn.z);		}	source.close();

}

void Calculate_Offset(void)
{
	// Calculate Offsets
	pthread_mutex_lock(&mutx.svs.pitch);	pthread_mutex_lock(&mutx.svn.pitch);	var.svo.pitch = var.svs.pitch - var.svn.pitch;	pthread_mutex_unlock(&mutx.svs.pitch);	pthread_mutex_unlock(&mutx.svn.pitch);
	pthread_mutex_lock(&mutx.svs.roll);		pthread_mutex_lock(&mutx.svn.roll);		var.svo.roll = var.svs.roll - var.svn.roll;		pthread_mutex_unlock(&mutx.svs.roll);	pthread_mutex_unlock(&mutx.svn.roll);
	pthread_mutex_lock(&mutx.svs.yaw);		pthread_mutex_lock(&mutx.svn.yaw);		var.svo.yaw = var.svs.yaw - var.svn.yaw;		pthread_mutex_unlock(&mutx.svs.yaw);	pthread_mutex_unlock(&mutx.svn.yaw);
	pthread_mutex_lock(&mutx.svs.w1);		pthread_mutex_lock(&mutx.svn.w1);		var.svo.w1 = var.svs.w1 - var.svn.w1;			pthread_mutex_unlock(&mutx.svs.w1);		pthread_mutex_unlock(&mutx.svn.w1);
	pthread_mutex_lock(&mutx.svs.w2);		pthread_mutex_lock(&mutx.svn.w2);		var.svo.w2 = var.svs.w2 - var.svn.w2;			pthread_mutex_unlock(&mutx.svs.w2);		pthread_mutex_unlock(&mutx.svn.w2);
	pthread_mutex_lock(&mutx.svs.w3);		pthread_mutex_lock(&mutx.svn.w3);		var.svo.w3 = var.svs.w3 - var.svn.w3;			pthread_mutex_unlock(&mutx.svs.w3);		pthread_mutex_unlock(&mutx.svn.w3);
	pthread_mutex_lock(&mutx.svs.vx);		pthread_mutex_lock(&mutx.svn.vx);		var.svo.vx = var.svs.vx - var.svn.vx;			pthread_mutex_unlock(&mutx.svs.vx);		pthread_mutex_unlock(&mutx.svn.vx);
	pthread_mutex_lock(&mutx.svs.vy);		pthread_mutex_lock(&mutx.svn.vy);		var.svo.vy = var.svs.vy - var.svn.vy;			pthread_mutex_unlock(&mutx.svs.vy);		pthread_mutex_unlock(&mutx.svn.vy);
	pthread_mutex_lock(&mutx.svs.vx);		pthread_mutex_lock(&mutx.svn.vx);		var.svo.vx = var.svs.vx - var.svn.vx;			pthread_mutex_unlock(&mutx.svs.vx);		pthread_mutex_unlock(&mutx.svn.vx);
	pthread_mutex_lock(&mutx.svs.x);		pthread_mutex_lock(&mutx.svn.x);		var.svo.x = var.svs.x - var.svn.x;				pthread_mutex_unlock(&mutx.svs.x);		pthread_mutex_unlock(&mutx.svn.x);
	pthread_mutex_lock(&mutx.svs.y);		pthread_mutex_lock(&mutx.svn.y);		var.svo.y = var.svs.y - var.svn.y;				pthread_mutex_unlock(&mutx.svs.y);		pthread_mutex_unlock(&mutx.svn.y);
	pthread_mutex_lock(&mutx.svs.z);		pthread_mutex_lock(&mutx.svn.z);		var.svo.z = var.svs.z - var.svn.z;				pthread_mutex_unlock(&mutx.svs.z);		pthread_mutex_unlock(&mutx.svn.z);

}


void Set_Actuator(void)
{
	// Determine if Actuator must actuate
	if ( fabs(var.svo.pitch)	> var.svt.pitch	||	fabs(var.svo.roll)		> var.svt.roll	||	fabs(var.svo.yaw)		> var.svt.yaw	||
		 fabs(var.svo.w1)		> var.svt.w1	||	fabs(var.svo.w2)		> var.svt.w2	||	fabs(var.svo.w3)		> var.svt.w3	||
		 fabs(var.svo.vx)		> var.svt.vx	||	fabs(var.svo.vy)		> var.svt.vy	||	fabs(var.svo.vz)		> var.svt.vz	||
		 fabs(var.svo.x)		> var.svt.x		||	fabs(var.svo.y)			> var.svt.y		||	fabs(var.svo.z)			> var.svt.z	)
	{
		pthread_mutex_lock(&mutx.act.shall.comm_orb); // Protect variables from overwriting
			printf("ORBIT: Requires Actuation.\n"); // Info
			var.act.shall.comm_orb = true; // Orbit wants to actuate
			sem_post(&act_queue); // Send petition to actuate
		pthread_mutex_unlock(&mutx.act.shall.comm_orb); // Unprotect variables from overwriting
	}

}