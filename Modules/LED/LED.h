/*
Function that will lit the led when required and will put it down.
*/

#include <wiringPi.h>

void Lit_LED(bool status, int pin)
{
    try {
        wiringPiSetup(); // Sets up wiring Pi
        if (pin<0 || pin>7) {throw pin;}
        pinMode(pin,OUTPUT); // Sets GPIO-17 as output
        
        // Switch to choose between the status to send a high level or a low level
        switch (status) {
            case false: // if a zero is received a low status is returned
                digitalWrite(pin,LOW);
                break;
            case true: // if a one is received a high level is returend
                digitalWrite(pin,HIGH);
                break;
                
            default: // by default a low level will be returned
                digitalWrite(pin,LOW);
                break;
        }
    } catch (int pin) {
        printf("Digital pin #%d is not recognized by Raspberry Pi.\n");
    }
}