// RasPiSat main program
//
// Simulates the controller for a small cubesat
// Check the other sources for specific info on the module
// ONLY works on Raspberry Pi

// Libraries definition
#include "Modules/libraries.h"

//Headers
void *dummy(void* parm); //Dummy function for thread
void *Watchdog(void*parm);
void NUKE_IT_FROM_ORBIT(void);

// Modules
#include "Modules/LED/LED.h"                    // LED communications function
#include "Modules/Actuator/actuator.h"          // Actuator thread
#include "Modules/Orbit/orbit.h"				// Orbital control thread
#include "Modules/Comms/coms_server_RasPiSat.h" // Communications server thread

int main()
{
	// Initialize thread attributes
    pthread_attr_init(&attr);
    sem_init(&act_queue,0,0);
    
    // Create threads
    pthread_create(&wdg,&attr,Watchdog,(void*) NULL);
    pthread_create(&crtl,&attr,Orbit,(void*) NULL);
    pthread_create(&coms,&attr,CommsServer,(void*) NULL);
    pthread_create(&act,&attr,Actuator,(void*) NULL);
    
    // Join all threads
    pthread_join(crtl,NULL);
    pthread_join(coms,NULL);
    pthread_join(act,NULL);
    pthread_join(wdg,NULL);
    
    // Kill all threads before exiting
    pthread_exit(NULL);
    
    return 0;
}

void *dummy(void* parm) //Dummy function for thread
{
    int i=0;
    
    while (1) {
        printf("Counting: %d\n",i);
        i++;
        sleep(5);
    }
}

// Watchdog Function
void *Watchdog(void*parm)
{
    // Non-stop bucle
	while(true)
	{
		// Wake up threads
		pthread_cond_signal(&mutx.orbit_cond); // Signal Orbit Thread

		// Update Clock
		time(&var.t.last);
		var.t.next = var.t.last + var.t.offset;
		
		// Show Time
		char buffer[4096];
		strftime (buffer,4096,"Last clock was %Z %T",gmtime(&var.t.last));
		puts(buffer);

		// Sleep until next iteration
		try
		{
			time_t now; time(&now); // Get time
			int insomnia = (int) ( var.t.next - now ); // Offset
			if(insomnia<0) throw(insomnia); // Error
			printf("WDG: Next Clock in: %d\n",insomnia); // Info
			sleep(insomnia); // Wait
		}

		catch (int insomnia) // Error
		{
			NUKE_IT_FROM_ORBIT(); // Blow Up Everything (TM)
		}
	}
}

// Critical Error
void NUKE_IT_FROM_ORBIT ( void )
{
    // Terminate all threads
    pthread_exit(NULL);
}
